import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Controls 2.3
import Qt.labs.calendar 1.0

Item {
    id: rectangle
    visible: true
    width: 640
    height: 480
    
    MouseArea{
        anchors.fill: parent
        onClicked: {
            focus = true
        }
    }
    property int m_mutex: 0
    function mutex(){
        if(m_mutex > 0)
        {
            m_mutex--;
            return false;
        }
        else{
            m_mutex++;
            return true;
        }

    }
    
    function convert(right)
    {
        if(mutex())
        {
            console.log(rightComboBox.currentText, leftComboBox.currentText, 
                        rightField.text, leftField.text)
            if(right)
                BankInfo.convert(true, rightComboBox.currentText, 
                                 leftComboBox.currentText, rightField.text)
            else
                BankInfo.convert(false, leftComboBox.currentText, 
                                 rightComboBox.currentText, leftField.text)
        }
    }

    Row {
        id: row
        height: childrenRect.height
        spacing: 50
        anchors.centerIn: parent
        
        Connections{
            target: BankInfo
            onLeftNumIsSet: {
                leftField.text = BankInfo.convertedNum
            }
            onRightNumIsSet: {
                rightField.text = BankInfo.convertedNum
            }
        }
        
        Row {
            id: leftRow
            width: childrenRect.width
            height: childrenRect.height
            
            ComboBox {
                id: leftComboBox
                width: 90
                model: currenciesCodesModel
                textRole: "display"
                onCurrentTextChanged: {
                    convert(false)
                }
            }
            
            TextField {
                id: leftField
                width: 150
                validator: DoubleValidator{bottom: 0}
                placeholderText: qsTr("")
                onTextChanged: {
                    convert(false)
                }
                onFocusChanged: {
                    if(!focus & text === "")
                        text = 0
                }
            }
        }
        
        Row {
            id: rightRow
            width: childrenRect.width
            height: childrenRect.height
            
            TextField {
                id: rightField
                width: 150
                validator: DoubleValidator{bottom: 0}
                placeholderText: qsTr("")
                onTextChanged: {
                    convert(true)
                }
                onFocusChanged: {
                    if(!focus & text === "")
                        text = 0
                }
            }
            
            ComboBox {
                id: rightComboBox
                model: currenciesCodesModel
                textRole: "display"
                width: 90
                onCurrentTextChanged: {
                    convert(false)
                }
            }
        }
    }
}
