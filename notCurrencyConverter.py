import requests
import sys
import os
import PySide2.QtQml
import xml.etree.ElementTree as ET
from PySide2.QtQuick import QQuickView
from PySide2.QtCore import QAbstractListModel, Qt, QUrl, QObject, Property, \
    QStringListModel, Signal, Slot
from PySide2.QtGui import QGuiApplication

url = "http://www.cbr.ru/scripts/XML_daily.asp?"
r = requests.get(url)
root = ET.fromstring(r.text)

class Currency(QObject):
    def __init__(self, currency=None, parent=None):
        if(currency!=None):
            self._name = currency.find("Name").text
            self._charCode = currency.find("CharCode").text
            self._nominal = float(currency.find("Nominal").text.replace(",",'.'))
            self._value = float(currency.find("Value").text.replace(",",'.'))

class BankInfo(QObject):
    def __init__(self, parent=None):
        QObject.__init__(self)
        self.m_currencies = {}
        for neighbour in root:
            currency = Currency(currency= neighbour)
            self.m_currencies[currency._charCode] = currency
        currency = Currency()
        currency._name = "Российский рубль"
        currency._charCode = "RUB"
        currency._nominal = 1
        currency._value = 1
        self.m_currencies[currency._charCode] = currency

    m_convertedNum = 0
    convertedNumChanged = Signal(float)
    leftNumIsSet = Signal()
    rightNumIsSet = Signal()
    @Property(float, notify=convertedNumChanged)
    def convertedNum(self):
        return self.m_convertedNum

    def charCodes(self):
        chCodes = []
        for charCode in self.m_currencies:
            chCodes.append(charCode)
        chCodes.sort()
        model = QStringListModel()
        model.setStringList(chCodes)
        return model

    @Slot(bool, str, str, float)
    def convert(self, right, from_code, to_code, num):
        print("right:", right, "fromCode:", from_code, "toCode:", to_code, "num:", num)
        fromCurrency = self.m_currencies[from_code]
        toCurrency = self.m_currencies[to_code]
        self.m_convertedNum = round(float(num) *
                                    (fromCurrency._value/fromCurrency._nominal) /
                                    (toCurrency._value/toCurrency._nominal), 5)
        print(self.m_convertedNum)

        self.convertedNumChanged.emit(self.m_convertedNum)
        if(right):
            self.leftNumIsSet.emit()
        else:
            self.rightNumIsSet.emit()

app = QGuiApplication(sys.argv)
view = QQuickView()
view.setResizeMode(QQuickView.SizeRootObjectToView)
bankInfo = BankInfo()
currenciesCodesModel = bankInfo.charCodes()
view.rootContext().setContextProperty("BankInfo", bankInfo)
view.rootContext().setContextProperty("currenciesCodesModel", currenciesCodesModel)
qml_file = os.path.join(os.path.dirname("__file__"), "notCurrencyConverter.qml")
view.setSource(QUrl.fromLocalFile(os.path.abspath(qml_file)))

if view.status == QQuickView.Error:
    sys.exit(-1)
view.show()

app.exec_()
del view